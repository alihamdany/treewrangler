# Coding Challenge

## Problem Statement

Take the  tree that has been generated for you and print out the nodes of the tree as a 
list of their hierarchies. For example:

![Default Tree](doc/tree.jpg?raw=true "Default Generated Tree")

This tree should generate a list of strings like so:

[
	"A.AA.AAA",
	"A.AA.AAB",
	"A.AB.ABA",
	"A.AB.ABB",
    "A.AB.ABC"
]

The tests should also be implemented to make sure that both the generated tree and
the generated lists are correct. Tests are found in `src/test/java/io/nimbusstack/MainTest.java`

## Installing and Running

Application uses Gradle to build. A gradle wrapper is included. To run the application:

#### Windows:

Main Application: `gradlew.bat run`

Tests: `gradlew.bat test`

#### Linux

Main Application `./gradlew run`

Tests: `./gradlew test`
