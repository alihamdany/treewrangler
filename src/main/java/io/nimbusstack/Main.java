package io.nimbusstack;

import io.nimbusstack.structure.Node;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Main {

    private static Node rootNode;

    public static void main(String[] args) {
        Node tree = generateTree();
        List<String> treeAsString = generateStringRepresentation(tree);
    }

    /**
     * Generates a static tree
     *
     * @return The root node of the tree
     */
    public static Node generateTree() {
        Node a = new Node("A", null);


        Node aa = new Node("AA", null);
        Node ab = new Node("AB", null);

        Node aaa = new Node("AAA", null);
        Node aab = new Node("AAB", null);
        //Node aad = new Node("AAD", null);


        Node aba = new Node("ABA", null);
        Node abb = new Node("ABB", null);
        Node abc = new Node("ABC", null);


        aa.setChildren(Arrays.asList(aaa, aab));
        ab.setChildren(Arrays.asList(aba, abb, abc));

        a.setChildren(Arrays.asList(aa, ab));

        return a;

    }

    /**
     * Return the given tree as a list of strings
     *
     * @param root The root node of the tree
     * @return The list of String that represent the tree
     */
    public static List<String> generateStringRepresentation(final Node root) {
        if(root == null)
            return null;

        List<String> listOfStrings = new ArrayList<String>();
        Stack nodeStack = new Stack();
        rootNode = root;
        nodeStack.push(root);
        traverse(root, nodeStack,listOfStrings);
        return listOfStrings;
    }


    /**
     * Traverses the tree and populates a list that contains the hiearchy string.
     *
     * @param node The root node of the tree, s Stack that keeps track of the nodes, listOfSTrings A list that will contai the hiearchy strings
     * @return A leaf node
     */
    public static Node traverse(Node node, Stack s, List<String> listOfStrings) {
        if (node.getChildren() == null) {
            Stack s1 = new Stack();
            s1 = s;
            listOfStrings.add(printStack(s1)) ;    // Populating the list with hierarchy string.
            s.pop();
            return node;
        }
        for (Node child : node.getChildren()) {    // Traversing the tree.
            s.push(child);
            traverse(child, s,listOfStrings);
        }
        s.clear();                                  // Emptying the stack after left side if done traversing
        s.push(rootNode);
        return null;
    }

    /**
     * Builds a hiearchy string from the elements of the stack.
     *
     * @param nodesToPrint A stack that contains the current hierarchy
     * @return A string that contains the hiearchy.
     */
    public static String printStack(Stack nodesToPrint) {

        Node[] nodeArray = new Node[nodesToPrint.size()];
        nodesToPrint.toArray(nodeArray);
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for (Node node : nodeArray) {                   // Building the string
            sb.append(prefix);
            prefix = ".";
            sb.append(node.getValue());
        }
        return sb.toString();
    }
}

