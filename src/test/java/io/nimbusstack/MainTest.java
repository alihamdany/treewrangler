package io.nimbusstack;

import io.nimbusstack.structure.Node;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MainTest {

    @Test
    public void testGenerateTree(){
        Node tree = Main.generateTree();
        List<String> treeAsString = Main.generateStringRepresentation(tree);
        Assert.assertFalse(treeAsString==null);
    }

    @Test
    // Testing the tree in the original problem
    public void testGenerateStringRepresentation(){
        Node tree = Main.generateTree();
        List<String> treeAsString = Main.generateStringRepresentation(tree);
        List<String> expectedoutput = Arrays.asList("A.AA.AAA","A.AA.AAB","A.AB.ABA","A.AB.ABB","A.AB.ABC");
        assertThat(treeAsString,is(expectedoutput));
    }

    @Test
    // Testing a symmetrical trees
    public void test1GenerateStringRepresentation() {
        Node a = new Node("A", null);
        Node aa = new Node("AA", null);
        Node ab = new Node("AB", null);
        Node aaa = new Node("AAA", null);
        Node aab = new Node("AAB", null);
        Node aad = new Node("AAD", null);
        Node aba = new Node("ABA", null);
        Node abb = new Node("ABB", null);
        Node abc = new Node("ABC", null);
        aa.setChildren(Arrays.asList(aaa, aab, aad));
        ab.setChildren(Arrays.asList(aba, abb, abc));
        a.setChildren(Arrays.asList(aa, ab));
        List<String> treeAsString = Main.generateStringRepresentation(a);
        List<String> expectedoutput = Arrays.asList("A.AA.AAA", "A.AA.AAB", "A.AA.AAD", "A.AB.ABA", "A.AB.ABB", "A.AB.ABC");
        assertThat(treeAsString, is(expectedoutput));
    }

    @Test
    // Testing tree with just the root node
    public void test2GenerateStringRepresentation() {
        Node a = new Node("A", null);
        List<String> treeAsString = Main.generateStringRepresentation(a);
        List<String> expectedoutput = Arrays.asList("A");
        assertThat(treeAsString, is(expectedoutput));
    }

    @Test
    // Testing left branch by itself
    public void test3GenerateStringRepresentation() {
        Node a = new Node("A", null);
        Node aa = new Node("AA", null);
        Node aaa = new Node("AAA", null);
        aa.setChildren(Arrays.asList(aaa));
        a.setChildren(Arrays.asList(aa));
        List<String> treeAsString = Main.generateStringRepresentation(a);
        List<String> expectedoutput = Arrays.asList("A.AA.AAA");
        assertThat(treeAsString, is(expectedoutput));
    }

    @Test
    // Testing right branch by itself
    public void test4GenerateStringRepresentation() {
        Node a = new Node("A", null);
        Node ab = new Node("AB", null);
        Node aba = new Node("ABA", null);
        ab.setChildren(Arrays.asList(aba));
        a.setChildren(Arrays.asList(ab));
        List<String> treeAsString = Main.generateStringRepresentation(a);
        List<String> expectedoutput = Arrays.asList("A.AB.ABA");
        assertThat(treeAsString, is(expectedoutput));
    }

    @Test
    // Test for NULL
    public void test5GenerateStringRepresentation() {

        List<String> treeAsString = Main.generateStringRepresentation(null);
        List<String> expectedoutput = null;
        assertThat(treeAsString, is(expectedoutput));
    }

    @Test
    // Testing a single node on left and multiple nodes on the right.
    public void test6GenerateStringRepresentation() {
        Node a = new Node("A", null);
        Node aa = new Node("AA", null);
        Node ab = new Node("AB", null);
        Node aba = new Node("ABA", null);
        Node abb = new Node("ABB", null);
        Node abc = new Node("ABC", null);
        ab.setChildren(Arrays.asList(aba, abb, abc));
        a.setChildren(Arrays.asList(aa, ab));
        List<String> treeAsString = Main.generateStringRepresentation(a);
        List<String> expectedoutput = Arrays.asList("A.AA","A.AB.ABA", "A.AB.ABB", "A.AB.ABC");
        assertThat(treeAsString, is(expectedoutput));
    }

    @Test
    // Testing with tree of depth 3.
    public void test7GenerateStringRepresentation() {
        Node a = new Node("A", null);
        Node aa = new Node("AA", null);
        Node ab = new Node("AB", null);
        Node aaa = new Node("AAA", null);
        Node aab = new Node("AAB", null);
        Node aba = new Node("ABA", null);
        Node abb = new Node("ABB", null);
        Node abc = new Node("ABC", null);
        Node abcd = new Node("ABCD", null);
        aa.setChildren(Arrays.asList(aaa, aab));
        ab.setChildren(Arrays.asList(aba, abb, abc));
        abc.setChildren(Arrays.asList(abcd));
        a.setChildren(Arrays.asList(aa, ab));
        List<String> treeAsString = Main.generateStringRepresentation(a);
        List<String> expectedoutput = Arrays.asList("A.AA.AAA","A.AA.AAB","A.AB.ABA","A.AB.ABB","A.AB.ABC.ABCD");
        assertThat(treeAsString, is(expectedoutput));
    }


}
